export interface FlowEventBase {
  type: string;
  timestamp: number;
}
export interface VisualUpdateEvent extends FlowEventBase {
  type: 'visualUpdate';
  beforeSnapshot: string;
  afterSnapshot: string;
}

export interface UserInteractionEvent extends FlowEventBase {
  type: 'userInteraction';
  interactionType: 'click';
}

export type FlowEvent = VisualUpdateEvent | UserInteractionEvent;

export interface TraceReport {
  events: FlowEvent[];
}