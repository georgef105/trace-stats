// For more info https://docs.google.com/document/d/1CvAClvFfyA5R-PhYUmn5OOQtYMH4h6I0nSsKchNAySU/preview
export interface Trace {
    metadata: any;
    traceEvents: Array<TraceEvent>;
}

export interface TraceEvent {
    pid: number;
    tid: number;
    ts: number;
    // B: begin, E: end, X: complete event, I: instant events(deprecated)
    ph: 'B' | 'E' | 'I' | 'X';
    cat: string;
    name: string;
    args: any;
}