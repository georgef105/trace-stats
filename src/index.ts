import { readFileSync } from 'fs';
import { getVisualEvents } from './visual-events';
import { getUserInteraction } from './user-interaction';
import { Trace } from './trace.interface';
import { FlowEvent } from './report.interface';

const _trace = JSON.parse(readFileSync('./reports/trace.json').toString());

async function getFlowEvents(trace: Trace): Promise<Array<FlowEvent>> {
  const visualEvents = await getVisualEvents(trace);
  const interactionEvent = getUserInteraction(trace);

  const events = [
    interactionEvent,
    ...visualEvents
  ];

  return Promise.resolve(events);
}

getFlowEvents(_trace).then(events => console.log('events', events));